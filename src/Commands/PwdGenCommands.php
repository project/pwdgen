<?php

namespace Drupal\pwdgen\Commands;

/**
 * @file
 * Drush command to generate password.
 */

use Drupal\pwdgen\PwdGenWordsService;
use Drush\Commands\DrushCommands;

/**
 * Class Password generator commands drush.
 *
 * @package Drupal\pwdgen\Commands
 */
class PwdGenCommands extends DrushCommands {

  /**
   * Password generator service.
   *
   * @var \Drupal\pwdgen\PwdGenWordsService
   */
  protected $pwdgen;

  /**
   * Initialise password generator commands drush.
   *
   * @param \Drupal\pwdgen\PwdGenWordsService $pwdgen
   *   Password generator service.
   */
  public function __construct(PwdGenWordsService $pwdgen) {
    parent::__construct();
    $this->pwdgen = $pwdgen;
  }

  /**
   * Drush command to generate password from phrase.
   *
   * @param string $text
   *   Phrase to encrypt must have at least 6 characters.
   * @param mixed[] $options
   *   List of options.
   *
   * @command pwdgen:generate
   * @aliases pwdgen
   * @option length
   *   Length of password.
   * @option option
   *   Option with password only or numbers only.
   *
   * @usage drush pwdgen:generate 'your text'
   *   Generate password with 12 characters.
   * @usage drush pwdgen:generate --length=20 'your text'
   *   Generate password with 20 characters.
   * @usage drush pwdgen:generate --option=symbols 'your text'
   *   Generate password with symbols only.
   * @usage drush pwdgen:generate --option=numbers 'your text'
   *   Generate password with numbers only.
   *
   * @throws \Exception
   */
  public function generate(
    string $text,
    array $options = [
      'length' => 12,
      'option' => '',
    ]
  ) {

    switch ($options['option']) {
      case 'numbers':
        $this->pwdgen->setSymbols(2);
        break;

      case 'symbols':
        $this->pwdgen->setSymbols(1);
        break;
    }

    if (strlen($text) > 5) {
      $password = $this->pwdgen->generatePassword($text, $options['length'], $options['length']);
      $this->output()->writeln($password);
    }
    else {
      throw new \RuntimeException(dt('Generation error please check command : drush pwdgen --help'));
    }
  }

}
