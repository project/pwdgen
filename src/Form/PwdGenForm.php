<?php

namespace Drupal\pwdgen\Form;

/**
 * @file
 * Password generator form settings.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pwdgen\PwdGenWordsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class password generator form.
 *
 * @package Drupal\pwdgen\Form
 */
class PwdGenForm extends FormBase {

  /**
   * Password generator service.
   *
   * @var \Drupal\pwdgen\PwdGenWordsService
   */
  protected PwdGenWordsService $pwdgen;

  /**
   * Initiate password generator form.
   *
   * @param \Drupal\pwdgen\PwdGenWordsService $pwdgen
   *   Password generator service.
   */
  public function __construct(PwdGenWordsService $pwdgen) {
    $this->pwdgen = $pwdgen;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pwdgen.genwords')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pwdgen.form'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pwdgen_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['words'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to encrypt'),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => '32',
      '#description' => $this->t('Generate password from one word or more, text must count at least 6 characters.'),
    ];

    $form['property'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Properties'),
    ];
    $form['property']['options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Include numbers and symbols'),
      '#options' => [
        '0' => $this->t('Both symbols and numbers'),
        '1' => $this->t('Symbols only'),
        '2' => $this->t('Numbers only'),
      ],
      '#default_value' => '0',
    ];

    $form['property']['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum password length'),
      '#default_value' => 8,
      '#size' => '4',
      '#min' => 6,
      '#description' => $this->t('Minimum password length should be at least 6 characters.'),
    ];

    $form['property']['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum password length'),
      '#default_value' => 32,
      '#size' => '4',
      '#max' => 64,
      '#min' => 6,
      '#description' => $this->t('Maximum password length should be greater than Minimum.'),
    ];

    $form['property']['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#default_value' => 10,
      '#size' => '4',
      '#min' => 1,
      '#max' => 20,
      '#description' => $this->t('Number of passwords to generate.'),
    ];

    $form['property']['dialogbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show result in dialog box'),
      '#default_value' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::ajaxGeneratePassword',
      ],
    ];

    $form['result'] = [
      '#type' => 'markup',
      '#markup' => '<div id="pwdgen-output"></div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax response generate password.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax data insert.
   *
   * @throws \Exception
   */
  public function ajaxGeneratePassword(array $form, FormStateInterface $form_state): AjaxResponse {

    $word = $form_state->getValue('words');
    $min = $form_state->getValue('min');
    $max = $form_state->getValue('max');
    $count = $form_state->getValue('count');
    $options = $form_state->getValue('options');
    $dialogbox = $form_state->getValue('dialogbox');

    if ($max < $min) {
      $generated_pwd = $this->t('Error : Maximum password length should be greater than Minimum.');
    }
    elseif (strlen($word) < 6) {
      $generated_pwd = $this->t('Error : Text field must have at least 6 characters.');
    }
    else {
      $this->pwdgen->setSymbols($options);
      $generated_pwd = '<table><tbody><th colspan="2">' . $this->t('Results') . '</th>';
      $list_pwd = [];
      for ($i = 0; $i < $count; $i++) {
        // Loop to skip repeated password.
        do {
          $pwd = $this->pwdgen->generatePassword($word, $min, $max);
        } while (in_array($pwd, $list_pwd, TRUE));
        $list_pwd[] = $pwd;
        $generated_pwd .= '<tr><th>' . $pwd . '</th><td>';
        $generated_pwd .= $this->t('(@n) Characters', ['@n' => strlen($pwd)]);
        $generated_pwd .= '</td></tr>';
      }
      $generated_pwd .= '</tbody></table>';
    }

    if ($dialogbox) {
      $dialogText['#attached']['library'][] = 'core/drupal.dialog.ajax';
      $dialogText['#markup'] = $generated_pwd;
    }

    $response = new AjaxResponse();
    if ($dialogbox) {
      $response->addCommand(new OpenModalDialogCommand($this->t('Passwords list'), $dialogText, ['width' => '400']));
      $response->addCommand(new HtmlCommand('#pwdgen-output', ''));
    }
    else {
      $response->addCommand(new HtmlCommand('#pwdgen-output', $generated_pwd));
    }

    return $response;

  }

}
