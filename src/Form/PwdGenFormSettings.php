<?php

namespace Drupal\pwdgen\Form;

/**
 * @file
 * Password Generator Form Settings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Password Generator Form Settings.
 *
 * @package Drupal\pwdgen\Form
 */
class PwdGenFormSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pwdgen.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pwdgen_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pwdgen.settings');
    $form['symbols'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Symbol text'),
      '#description' => $this->t('List of symbols to use as replacement of crypted text.'),
      '#default_value' => $config->get('symbols'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];
    
    $form['actions']['revert'] = [
      '#type' => 'submit',
      '#value' => $this->t('Revert to default'),
      '#submit' => ['::revert'],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $symbol = str_replace(' ', '', $form_state->getValue('symbols'));
    $config = $this->configFactory->getEditable('pwdgen.settings');
    $config->set('symbols', $symbol)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Add revert action of symbols.
   */
  public function revert(array &$form, FormStateInterface $form_state) {
    $default_value = '&~#<>{}+-*=\\/?!|@()$%_';

    $config = $this->configFactory->getEditable('pwdgen.settings');
    $config->set('symbols', $default_value)
      ->save();

    $this->messenger()->addMessage($this->t('Configuration reverted to default value.'));
  }

}
