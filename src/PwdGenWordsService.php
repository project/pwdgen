<?php

namespace Drupal\pwdgen;

/**
 * @file
 * Generate a random text from a selected word.
 */

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service random generator from a selected word.
 *
 * @package Drupal\pwdgen
 */
class PwdGenWordsService {

  /**
   * List of symbols.
   *
   * @var array
   */
  private array $symbols;

  /**
   * List of replacement of defined char.
   *
   * @var array
   */
  private array $replacement;

  /**
   * Include numbers or not.
   *
   * @var bool
   */
  private bool $hasNumber = TRUE;

  /**
   * Include symbols or not.
   *
   * @var bool
   */
  private bool $hasSymbol = TRUE;

  /**
   * Configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Initiate Password Generator Service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->setSymbols();
    $this->setReplacement();
  }

  /**
   * Generate Password from word.
   *
   * @param string $word
   *   The word to encrypt.
   * @param int $min
   *   Minimum of characters.
   * @param int $max
   *   Maximum of characters.
   *
   * @return string
   *   Crypt password generated.
   *
   * @throws \Exception
   */
  public function generatePassword(string $word, int $min, int $max): string {

    $return = '';
    if ($word) {
      $word = strtolower($word);
      $word = str_replace(' ', '', $word);
      $word = $this->unAccent($word);
      $chars = str_split($word);
      $replacement = $this->replacement;
      for ($i = 0; $i < $min; $i++) {
        $times = random_int(0, 5);
        if (array_key_exists($i, $chars) && strlen($return) <= $max) {
          if (array_key_exists($chars[$i], $replacement)) {
            $return .= $this->getSimilarChar($chars[$i]);
          }
          elseif ($times == 1) {
            $return .= $this->getUpper($chars[$i]);
          }
          elseif ($times == 2) {
            $return .= $this->getUpper($chars[$i]);
            $return .= $this->getAdded($chars[$i]);
          }
          else {
            $return .= $chars[$i];
          }
        }
        elseif (strlen($return) <= $max) {
          $random = random_int(0, count($this->symbols) - 1);
          $return .= $this->symbols[$random];
        }
      }
    }
    return substr($return, 0, $max);
  }

  /**
   * Replace char with similar sign.
   *
   * @param string $char
   *   The character.
   *
   * @return false|string
   *   Value shuffled
   */
  private function getSimilarChar(string $char) {
    $return = FALSE;
    if (strlen($char) === 1) {
      $chars = $this->replacement;
      if (array_key_exists($char, $chars)) {
        shuffle($chars[$char]);
        $return = $chars[$char][0];
      }
    }
    return $return;
  }

  /**
   * Upper of characters.
   *
   * @param string $char
   *   The character.
   *
   * @return string
   *   Upper char.
   */
  private function getUpper(string $char) {
    return strtoupper($char);
  }

  /**
   * Get added symbols.
   *
   * @param string $char
   *   The character.
   *
   * @return string
   *   Return added symbols.
   */
  private function getAdded(string $char) {
    shuffle($this->symbols);
    $times = random_int(1, 5);
    $return = (random_int(0, 1) == 1) ? $char : '';
    for ($i = 0; $i < $times; $i++) {
      $return .= $this->symbols[$i];
    }
    return $return;
  }

  /**
   * Set list of symbols.
   *
   * @param int $option
   *   Options number.
   */
  public function setSymbols(int $option = 0) {
    $config = $this->configFactory->get('pwdgen.settings');
    switch ($option) {
      case 1:
        $this->hasNumber = FALSE;
        break;

      case 2:
        $this->hasSymbol = FALSE;
        break;
    }

    $number = ($this->hasNumber) ? range(0, 9) : [];
    $symbol = ($this->hasSymbol) ? $config->get('symbols') : '';
    $this->symbols = array_merge($number, str_split($symbol));
    $this->setReplacement();
  }

  /**
   * Set list of replacement vowels.
   */
  public function setReplacement(): void {
    // Use vowels replacement.
    $replacement = [
      'a' => ['1', '&', '@', '2'],
      'e' => ['&', '€', '9', '6', '~'],
      'i' => ['l', '1', '!', '?', '|', '#'],
      'o' => ['0', '@', '(', ')', '8'],
      'y' => ['l', '1', '!', '?', '|', '#'],
      'u' => ['l', '1', '!', '?', '|', '#'],
    ];

    foreach ($replacement as $key => $value) {
      $replacement[$key] = array_intersect($value, $this->symbols);
      array_unshift($replacement[$key], strtolower($key), strtoupper($key));
    }
    $this->replacement = $replacement;
  }

  /**
   * Get Replacement.
   *
   * @return array
   *   The replacement.
   */
  public function getReplacement(): array {
    return $this->replacement;
  }

  /**
   * Transform known accent to normal characters.
   *
   * @param string $string
   *   Accent string.
   *
   * @return string
   *   String with no accent.
   *
   * @see https://stackoverflow.com/questions/12697107/replace-accented-characters#answer-12698997
   */
  public function unAccent(string $string): string {
    return preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
  }

}
