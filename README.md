CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration
* Drush
* Maintainers

INTRODUCTION
------------

Password generator module provides a custom password from a text or set of words
to memorize a thin part with moderate complexity.

This module allows you to choose encryption within a character number limit with
the following properties: symbols and numbers


* For a full description of the module, visit the project page:
  https://www.drupal.org/project/pwdgen

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/pwdgen

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

Download and install the pwdgen module.
* Once the module installed, go to /admin/config/people/password-generate/settings
  and configure symbol text.

* Configure the user permissions in Administration » People » Permissions:
  Go to admin/people/permissions#pwdgen


DRUSH
-------------

Drush command to generate password from phrase.

Examples:
 * drush pwdgen:generate 'your text'
   Generate password with 12 characters.
 * drush pwdgen:generate --length=20 'your text'
    Generate password with 20 characters.
 * drush pwdgen:generate --option=symbols 'your text'
    Generate password with symbols only.
 * drush pwdgen:generate --option=numbers 'your text'
    Generate password with numbers only.

Arguments:
 * Phrase to encrypt must have at least 6 characters.

Options:
 * --length[=LENGTH] Length of password. [default: 12]
 * --option[=OPTION] Option with password only or numbers only.

Aliases: pwdgen

MAINTAINERS
-----------

Current maintainers:
* Khalil Charfi (ckhalilo) - https://www.drupal.org/u/ckhalilo
